package util;
/**
 * Stored a number mod modulus.
 * 
 * @author Chuck Cusack, January 2013.
 */
public class RangedNumber {
	private int	modulus;
	private int	number;

	/**
	 * Create a new RangedNumber with value 0 and modulus mod.
	 * 
	 * @param mod the modulus
	 */
	public RangedNumber(int mod) {
		this.modulus = mod;
	}

	/**
	 * Create a new RangedNumber with value number and modulus mod.
	 * 
	 * @param mod the modulus
	 * @param number the value of the number.
	 */
	public RangedNumber(int mod, int number) {
		this.modulus = mod;
		// Call the setter in case number>=mod.
		setNumber(number);
	}

	public int getModulus() {
		return modulus;
	}

	public int getNumber() {
		return number;
	}

	/**
	 * Set the number to number.
	 * 
	 * @param number
	 * @return true if the number was too big and was modded and false otherwise.
	 */
	// This method contains one or more subtle errors but they are left
	// because they are perfect for seeing why testing is important.
	public boolean setNumber(int number) {
		if (number < modulus) {
			this.number = number;
			return false;
		} else {
			this.number = number % modulus;
			return true;
		}
	}

	/**
	 * Subtract one from the number, wrapping around to (modulus-1) if the number becomes negative.
	 * 
	 * @return true if the number wrapped around and became (modulus-1), false otherwise.
	 */
	public boolean decrement() {
		number--;
		if (number < 0) {
			number = modulus - 1;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Add one to the number, wrapping around to 0 if the number becomes equal to the modulus.
	 * 
	 * @return true if the number wrapped around to 0, false otherwise.
	 */
	public boolean increment() {
		number++;
		if (number == modulus) {
			number = 0;
			return true;
		} else {
			return false;
		}
	}

}
